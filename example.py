import random


def ugadayka():  # Вставить код для обработки ошибок
    """Угадайка"""
    number = random.randint(0, 100)
    while True:
        answer = input('Угадайте число: ')
        # если ввести не число, то всё поломается. Обработку ошибок рассмотрим позже
        if answer == 'exit':
            print('Завершена подпрограмма 1')
            raise SystemExit(1)
        elif answer == '!q':
            break
        answer = int(answer)

        if answer == number:
            print('Успех')
            break

        elif answer < number:
            print('Бери выше')
        else:
            print('Бери ниже')


def shout_on_me():
    """кричалка"""
    answer = input('Что ты сказал? ')
    while True:
        if answer == 'exit':
            print('Завершена подпрограмма 2')
            raise SystemExit(2)
        elif answer == '!q':
            break
        answer = input('Сам ты %s. И не кричи на меня\n\n' % answer.upper())


def culc():
    """калькулятор"""
    while True:
        print('Введите выражение: ')
        answer = input()
        if answer == 'exit':
            print('Завершена подпрограмма 3')
            raise SystemExit(3)
        elif answer == '!q':
            break
        a, b = answer.split('+')
        print(int(a) + int(b))
        print()


def main():
    """Определятор"""
    while True:
        answer = input('Определятор: \n'
                       '1. угадайка\n'
                       '2. покричи\n'
                       '3. калькулятор\n')
        if answer == 'exit':
            break
        dict_commands = {'1': ugadayka, 'УГАДАЙКА': ugadayka,
                         '2': shout_on_me, 'ПОКРИЧИ': shout_on_me,
                         '3': culc, 'КАЛЬКУЛЯТОР': culc}
        client_handler = dict_commands.get(answer.upper(), 'error')
        client_handler()


if __name__ == '__main__':
    main()